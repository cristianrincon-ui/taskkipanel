jQuery(document).ready(function($) {
	// Animations
	//-----------------------------------------------
	var delay=0, setTimeoutConst;
	if (($("[data-animation-effect]").length>0) && !Modernizr.touch) {
		$("[data-animation-effect]").each(function() {
			var item = $(this),
			animationEffect = item.attr("data-animation-effect");

			if(Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
				item.appear(function() {
					if(item.attr("data-effect-delay")) item.css("effect-delay", delay + "ms");
					setTimeout(function() {
						item.addClass('animated object-visible ' + animationEffect);

					}, item.attr("data-effect-delay"));
				}, {accX: 0, accY: -130});
			} else {
				item.addClass('object-visible');
			}
		});
	};

	// Navbar
	//-----------------------------------------------
	var scroll_start = 0;
   	var startchange = $('#home');
   	var offset = startchange.offset();
    if (startchange.length){
	   	$(document).scroll(function() { 
	      	scroll_start = $(this).scrollTop();
	      	if(scroll_start > offset.top - 0) {
	          	$(".navbar-default").addClass('dark');
	          	$(".navbar-brand > img").attr('src', 'resources/img/logo-green.svg');
	       	} else {
	          	$('.navbar-default').removeClass('dark');
	          	$(".navbar-brand > img").attr('src', 'resources/img/logo-green.svg');
	       }
	   	});
    }
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 0
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });

    // Carousel
	//-----------------------------------------------
    $('.multi-item-carousel').carousel({
      interval: 5000
    });

    $('.multi-item-carousel .item').each(function(){
      var next = $(this).next();
      if (!next.length) {
        next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
      
      if (next.next().length>0) {
        next.next().children(':first-child').clone().appendTo($(this));
      } else {
      	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
      }
    });

    // Table links
	//-----------------------------------------------
	$('table').on('click', 'tr[data-link]', function() {
		var link = $(this).attr('data-link');
		window.location = link;
	});

});

// Table links
//-----------------------------------------------
function togglePopOver(id, toggle) {
	var target = $(id);
	var toggle = $(toggle)
	var active = $(".popOver.activePopOver:not('"+id+"')").length;

	if (active != 0) {
		$(".activePopOver").addClass("slideOutUp").removeClass('slideInDown activePopOver');
		$("[data-type='togglePop']").removeClass('active')
	}

	if (target.hasClass("activePopOver")) {
		target.addClass("slideOutUp").removeClass('slideInDown');
		setTimeout(function() {
			target.removeClass('activePopOver');
		}, 100)
	} else {
		target.addClass('slideInDown activePopOver').removeClass('slideOutUp');
	}
	toggle.toggleClass('active');
}